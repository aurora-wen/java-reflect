package com.wen.reflect;

import com.wen.reflect.SayHi;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflect_demo {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        //得到Class类对象
        Class<SayHi> sayHiClass = SayHi.class;
        Class<?> sayHiClass1 = Class.forName("com.wen.reflect.SayHi");

        //获取构造器
        Constructor<SayHi> constructor = sayHiClass.getConstructor();

        //获取方法
        Method hi = sayHiClass.getMethod("Hi", String.class);

        //创建对象实例
        SayHi sayHi = constructor.newInstance();

        //执行方法
        Object result = hi.invoke(sayHi, "wen");

        System.out.println("执行结果：" + result);
    }
}
